#include <stdio.h>
#include "main.hpp"

Attacker::Attacker(float power) : power(power) {
}

void Attacker::attack(Actor *owner, Actor *target) {
	if ( target->destructible ) {
		target->destructible->takeDamage(target,power,owner);
	} else {
		printf ("%s attacks %s in vain.\n",owner->name,target->name);
	}
}
